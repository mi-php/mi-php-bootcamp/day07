<?php
class Jaime {
    public function sleepWith($check)
    {
        if ($check instanceof Tyrion)
        {
            echo "Not even if I'm drunk !\n";
        }
        else if ($check instanceof Lannister)
        {
            echo "With pleasure, but only in a tower in Winterfell, then.\n";
        }
        else
        {
            echo "Let's do this.\n";
        }
    }
}
?>